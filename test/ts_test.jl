
function test_psd(tl, dt, mean_val)
    n = 16
    N = round(Int,n*tl/dt)
    ts = mean_val .+ randn(N).*0.2
    @show std(ts)
    P = welch_pgram(ts, N÷n, 0, fs=1/dt, nfft=N÷n)
    @show sqrt(mean(P.power[2:end])*1/2dt)
    return P
end

function test_ts_gen()
    f_TLS = 5.0
    tl = 1000
    dt = 0.5
    h0_f = (0.001)^2*2*dt
    wanderoo = rand(-20:-0.125:-24)
    f_ts = random_pink_noise_ts(tl, dt, f_TLS, h0_f, h0_f/dt, 2.0^wanderoo)
    t = dt : dt : tl
    return t, f_ts
end
