using QubitFluctuations, QubitFluctuations.Distributions, Random, Test


p_max = 6.0

p_min = exp10.(range(-3,0,length=100))

let C = 0
for p_min in p_min
    d = QubitFluctuations.DipoleDist(p_min,p_max)
    dbig = QubitFluctuations.DipoleDist(big(p_min),big(p_max))
    for p in range(p_min,p_max,length=10001)
        v = cdf(d,p)
        vbig = Float64(cdf(dbig,big(p)))
        if abs(v - vbig)/vbig > 5e-13
            #@show p_min, p
            C += 1
        end
    end
end
println("count: $C")
end
