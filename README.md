# QubitFluctuations.jl

This package contains functions to simulate Qubit noise caused by the interaction between so-called thermal Two-Level Systems (T-TLS) and quantum Two-Level Systems (Q-TLS). T-TLSs are simulated as simple left/right well states that change the frequency of the Q-TLS they are coupled to. In turn, the Q-TLSs interact with a qubit, resulting in time fluctuations.

## Examples

The `script` folder contains several scripts showing examples of how the use the functions included in this package. Of note, the `t1_sim.jl` file shows how to generate a distribution of Q-TLSs and simulate the effect of fluctuations on the qubit. Other files relate to certain specific aspects of the simulations, such as the qubit E-field or the distributions of T-TLS parameters.

![T1_v_time](T1_v_time.png)
