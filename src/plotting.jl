using PyPlot

function heatmap(x, y, Z; lim=nothing, ar=3/2, cmap="viridis", cnorm=:normalize, extend="neither",
                 xlabel=nothing, ylabel=nothing, clabel=nothing, impath=nothing)
    # colormap limits, either given or computed from data
    vmin, vmax = (lim === nothing ? extrema(filter(!isnan, Z)) : lim)
    # axis ratio
    R = (x[end]-x[1])/(y[end]-y[1])
    # axis extent
    Δx, Δy = x[2]-x[1], y[2]-y[1] # assume uniform sampling
    ext = [x[1]-Δx/2, x[end]+Δx/2, y[1]-Δy/2, y[end]+Δy/2]
    # colormap normalization
    norm = cnorm == :normalize ? matplotlib.colors.Normalize(vmin, vmax) :
           cnorm == :lognorm   ? matplotlib.colors.LogNorm(vmin, vmax)   :
           error("invalid colormap normalization, choose `:normalize` or `:lognorm`")
    # plot
    im = imshow(Z, extent=ext, origin="lower", aspect=R/ar, cmap=cmap, norm=norm, interpolation="nearest")
    # colorbar
    cbar = colorbar(im, extend=extend, fraction=0.046, aspect=20/ar, pad=0.04)
    # labels & misc.
    xlabel === nothing || PyPlot.xlabel(xlabel)
    ylabel === nothing || PyPlot.ylabel(ylabel)
    clabel === nothing || cbar.set_label(clabel)
    tight_layout()
    impath === nothing || imsave(impath, Z, origin="lower", vmin=vmin, vmax=vmax, cmap=cmap)
    return im
end
