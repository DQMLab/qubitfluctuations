function relax_rate_barends(f_TLS, g, f_qubit, γ, κ)
    # this function assumes natural frequency units (not radial) for all quantities
    # assumes bare qubit relaxation rate γ is 0
    Δ  = (f_qubit - f_TLS)*2π
    g² = (g*2π)^2
    return g²*κ/(Δ^2 + κ^2/4)
end

function relax_rate(f_TLS, g, f_qubit, γ, κ)
    # this function assumes natural frequency units (not radial) for all quantities
    # more accurate than the one in Barends 2013
    Δ  = (f_qubit - f_TLS)*2π
    g² = (g*2π)^2
    x = (κ-γ)^2/4 - 4g² - Δ^2
    y = Δ*(γ-κ)
    reB2 = _real_sqrt(x,y)
    p = ifelse(κ>γ, -1, 1)
    return (κ-γ)/2 + p*reB2
end

function _real_sqrt(x,y)
    # requires IfElse.ifelse (included with LoopVectorization) to work
    t = sqrt(2*(abs(x) + hypot(x, y)))
    return LoopVectorization.ifelse(x ≥ 0, t/2, abs(y)/t)
end

function qubit_v_time(tl::Real, dt::Real, f_TLS::Real, g::Real, f_qubit, Γ_qubit, Γ_TLS, params...; showplot=false)
    # Vary f_TLS over time and compute Γ₁ & Δf_qubit
    f_ts = tls_freq_v_time(f_TLS,tl,dt)
    if showplot
        figure(12,figsize=(9,7))
        plot(0:dt/3600:tl/3600,f_ts.*1E-3)
        xlabel("Time (h)"); ylabel("TLS Frequency (GHz)")
        grid(true); tight_layout()
    end
    Γ  = @avx relax_rate.(f_ts, g, transpose(f_qubit), Γ_qubit, Γ_TLS, params...)
    Δf = @avx      shift.(f_ts, g, transpose(f_qubit), Γ_qubit, Γ_TLS, params...)
    return Γ, Δf
end

function qubit_v_time(tl::Real, dt::Real, f_TLS::Vector, g::Vector, f_qubit, Γ_qubit, fit_err_Γ=0.002, fit_err_f=0.014, params...)
    # white noise on Γ and Δf to simulate the fitting error, ~2kHz & ~14kHz
    # TODO: add 1/f flux noise on Δf as well?
    Γ  = reduce(hcat, [random_noise_ts(tl, dt, Γ_qubit, fit_err_Γ^2*2*dt, 0, 0) for i=1:length(f_qubit)])
    Δf = reduce(hcat, [random_noise_ts(tl, dt,     0.0, fit_err_f^2*2*dt, 0, 0) for i=1:length(f_qubit)])
    # threaded mapreduce, see functions below
    ts = tmapreduce(add2!, zip(f_TLS,g)) do (f_TLS,g)
        # add up the contribution of each high-freq TLS on the qubit
        # Γ_TLS depends on Δ₀ as Γ_TLS ∝ Δ₀²*Γ_TLS_max
        Γ_TLS_max = 100.0
        Γ_TLS = rand(InverseDist(0.1,1.0))^2 * Γ_TLS_max # https://doi.org/10.1063/1.5096182
        return qubit_v_time(tl, dt, f_TLS, g, f_qubit, Γ_qubit, Γ_TLS, params...)
    end
    add2!((Γ,Δf), ts)
    return Γ, Δf
end

# similar as the above but meant to be used with already-created TLS frequency time series
function qubit_v_time(tl::Real, dt::Real, f_TLS_ts::Matrix, g::Vector, Γ_TLS::Vector, f_qubit, Γ_qubit, fit_err_Γ=0.002, fit_err_f=0.014, params...)
    # white noise on Γ to simulate the fitting error, ~2kHz
    Γ  = reduce(hcat, [random_noise_ts(tl, dt, Γ_qubit, fit_err_Γ^2*2*dt, 0, 0) for i=1:length(f_qubit)])
    Δf = reduce(hcat, [random_noise_ts(tl, dt,     0.0, fit_err_f^2*2*dt, 0, 0) for i=1:length(f_qubit)])
    # add up the contribution of each high-freq TLS on the qubit
    for i in 1:size(f_TLS_ts,2)
        f_ts = @views f_TLS_ts[:,i]
        Γ  .+= relax_rate.(f_ts, g[i], transpose(f_qubit), Γ_qubit, Γ_TLS[i], params...)
        Δf .+=      shift.(f_ts, g[i], transpose(f_qubit), Γ_qubit, Γ_TLS[i], params...)
    end
    return Γ, Δf
end

function shift(f_TLS, g, f_qubit, γ, κ)
    # frequency shift caused by a TLS to an anharmonic oscillator
    # shift = f_qubit(TLS=0) - f_qubit(no TLS)
    Δ = f_TLS - f_qubit
    return (Δ - sign(Δ)*√(4g^2+Δ^2))/2
end

# threaded mapreduce utilties
tmapreduce(f::F, rf::R, xs; init=nothing) where {F,R} = foldxt(rf, Map(f), xs; init=init)

add2!(a::Nothing, b::Tuple{T,T}) where {T} = b
function add2!(a::Tuple{T,T}, b::Tuple{T,T}) where {T}
    a[1].+=b[1]
    a[2].+=b[2]
    return a
end
