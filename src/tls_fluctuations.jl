# energy units in MHz
# distribution for the low-energy TLF parameters Δ & Δ₀
const Δ_min, Δ_max, μ = 125E0, 1E3, 0.3
const DΔ = GTMDist(Δ_min,Δ_max,μ)
const Ω₀ = 1E3 # Well frequency, aka attempt frequency
const K = 8E3 # K = 1/2m*(ħ/d)², 8GHz for m = 16u, d = 2Å
const T = 1250 # 60mK in MHz
# adjust switching rates
const E_max  = T/2 # maximum allowed energy for a TLF to be activated
const γ_max  = 1E-2 # fastest allowed switching rate, when Δ₀ = E_max
const log_γ₀ = K/T*log(Ω₀/E_max)^2 + log(γ_max)
# distribution of interaction distances r in a puck-shaped volume
const U₀ = 200E3 # 200 GHz*nm^3 ~ 10K*nm^3, U = U₀/r^3
const Dr = LinearDist(15,60) # 60nm maximum radius, U(r_max) ≈ 1MHz

# 300K ~ 6000 GHz
# 10mK ~ 200 MHz
# 0.2mK ~ 4 MHz

function tls_freq_v_time(f_TLS,tl,dt)
    # for a particular TLS, simulate the interaction with n TLFs, e.g. the effect on the TLS frequency
    # pick a number of strongly coupled TLFs
    n_TLF = 10
    # create the initial time series for the TLS frequency
    h0_f = 0.1^2*2*dt # white noise to simulate (crudely) qubit frequency noise
    f_ts = random_noise_ts(tl, dt, f_TLS, h0_f, 0, 0)
    n = 0
    while n < n_TLF
        # pick Δ & Δ₀
        Δ,Δ₀ = rand(DΔ) # TLF asymmetry & tunnel splitting
        E = √(Δ^2+Δ₀^2) # unperturbed TLF energy, E_TLF ≪ E_TLS
        U = U₀/rand(Dr)^3 # interaction strength, U = U₀/r^3
        if √(E^2 + 4U*(Δ+U)) > E_max # not thermally activated
            continue
        end
        # compute switching rate
        V = K*log(Ω₀/Δ₀)^2 # barrier height
        γ = exp(-(V+Δ)/T+log_γ₀) # switching rate (activation energy Eₐ = V + Δ)
        # got a TLF, apply RTS frequency shift
        δf = tls_tls_shift(U,E,Δ)
        add_rts_noise!(f_ts,dt,1/γ,1/γ,δf)
        n += 1
        #@show E, Δ, Δ₀, γ, U, δf
    end
    return f_ts
end


function tls_freq_v_time(f_TLS, tl, dt, γ, δf)
    # for a particular TLS, simulate the interaction with n TLFs, e.g. the effect on the TLS frequency
    # this method is for specified switching rate γ and shift δf
    # pick a number of strongly coupled TLFs
    n_TLF = length(γ)
    # create the initial time series for the TLS frequency
    h0_f = 0.1^2*2*dt # white noise to simulate (crudely) qubit frequency noise
    f_ts = random_noise_ts(tl, dt, f_TLS, h0_f, 0, 0)
    for n in 1:n_TLF # apply TLF fluctuations
        add_rts_noise!(f_ts,dt,1/γ[n],1/γ[n],δf[n])
    end
    return f_ts
end

function tls_tls_shift(U, E, Δ)
    # absolute energy shift caused by a low energy TLS to a high energy TLS,
    # shift is negative (e) or positive (g) depending on low energy TLS state
    # U is the interaction energy between the two, normally U(r) = U₀/r^3
    # E = √(Δ^2+Δ₀^2) is the low frequency TLS energy, with asymmetry energy Δ
    # for small interaction strength U, the shift is:
    # Δf ~ 2*U*Δ/E + O(U^3)
    # for small asymmetry Δ, the shift is
    # Δf ~ 2*U*Δ/√(Δ₀^2+4*U^2) - O(Δ^3)
    # for small tunneling energy Δ₀, the shift is:
    # Δf ~ 2*U - O(Δ₀^2)
    return √((E/2)^2 + U*Δ + U^2) - √((E/2)^2 - U*Δ + U^2)
end

function tls_tls_eigenen(n, k, E₀=5, U=1E-3, E=5E-3, Δ=0.9*E)
    # n is the TLS level (0,1), k denotes the low energy TLS induced splitting (-1,+1)
    # E₀ is the high frequency TLS energy
    # U is the interaction energy between the two, normally U(r) = U₀/r^3
    # E is the low frequency TLS energy, with asymmetry energy Δ
    n == 0 || n == 1 || throw(ArgumentError("invalid n parameter, n must be ∈ (0,1)"))
    abs(k) == 1 || throw(ArgumentError("invalid k parameter, k must be ±1"))
    E₀/E > 10 || throw(ArgumentError("E₀ must be much greater than E"))
    #
    s = 2n-1 # (0,1) -> (-1,1)
    return s*E₀/2 + k * √((E/2)^2 - s*U*Δ + U^2)
end
