using Distributions
using Distributions: @distr_support, @quantile_newton, quantile_newton,
    cquantile_newton, invlogcdf_newton, invlogccdf_newton
using Random: AbstractRNG
import Distributions: params, insupport, _pdf, _rand!
# We model the distribution of TLSs in the General Tunelling Model (GTM) as done in
# Faoro & Ioffe (2015). https://doi.org/10.1103/PhysRevB.91.014201

struct GTMDist{T<:Real} <: ContinuousMultivariateDistribution
    Δ_min::T
    Δ_max::T
    μ::T
end

function insupport(d::GTMDist, X::AbstractVector{T}) where T<:Real
    Δ,Δ₀ = X
    Δ_min, Δ_max, μ = d.Δ_min, d.Δ_max, d.μ
    if 0 <= Δ <= Δ_max && Δ_min <= Δ₀ <= Δ_max
        return true
    else
        return false
    end
end

params(d::GTMDist) = (d.Δ_min, d.Δ_max, d.μ)

Base.length(d::GTMDist) = 2

Base.eltype(::Type{GTMDist{T}}) where {T} = T

function _pdf(d::GTMDist, X::AbstractVector{T}) where T<:Real
    if insupport(d, X)
        Δ,Δ₀ = X
        Δ_min, Δ_max, μ = d.Δ_min, d.Δ_max, d.μ
        A = Δ_max * log(Δ_max/Δ_min)
        return 1/A * (1+μ)*(Δ/Δ_max)^μ * 1/Δ₀
    else
        return zero(T)/one(T)
    end
end

function _rand!(rng::AbstractRNG, d::GTMDist, x::AbstractVector{<:Real})
    Δ_min, Δ_max, μ = d.Δ_min, d.Δ_max, d.μ
    Δ  = quantile_asym_dist(Δ_max, μ, rand(rng))
    Δ₀ = quantile_inverse_dist(Δ_min, Δ_max, rand(rng))
    x .= (Δ,Δ₀)
end

quantile_asym_dist(Δ_max::Real, μ::Real, q::Real) = Δ_max*q^(1/(1+μ))
