using Distributions
using Distributions: @distr_support, @quantile_newton, quantile_newton,
    cquantile_newton, invlogcdf_newton, invlogccdf_newton
import Distributions: params, pdf, cdf, minimum, maximum, mode, quantile,
    cquantile, invlogcdf, invlogccdf
# we model the distribution of TLS dipole moments in AlOx according to a scaled
# square root distribution

struct DipoleDist{T<:Real} <: ContinuousUnivariateDistribution
    p_min::T
    p_max::T
end

@distr_support DipoleDist d.p_min d.p_max

params(d::DipoleDist) = (d.p_min, d.p_max)

mode(d::DipoleDist) = d.p_min

function pdf(d::DipoleDist, p::Real)
    # Barends et al. 2013, supplementary
    if insupport(d, p)
        p_min, p_max = d.p_min, d.p_max
        B = sqrt(1 - (p_min/p_max)^2)
        C = sqrt(1 - (p/p_max)^2)
        A = log((B+1)*p_max/p_min) - B
        return 1/A * C/p
    else
        return zero(1/p)
    end
end

function cdf(d::DipoleDist, p::T) where T<:Real
    if insupport(d, p)
        p_min, p_max = d.p_min, d.p_max
        xm = (p_min/p_max)^2
        xp = (p/p_max)^2
        B = sqrt(1 - xm)
        C = sqrt(1 - xp)
        A = log((B+1)*p_max/p_min) - B
        if 10*p_min < p_max && !(T <: BigFloat)
            BB = 4/xm - @evalpoly(xm,T(2),T(1)/4,T(1)/8,T(5)/64,T(7)/128,T(21)/512,T(33)/1024)
        else
            BB = (1+B)/(1-B)
        end
        if 12*p < p_max && !(T <: BigFloat)
            # use approximation to avoid cancellation for small p
            CC = @evalpoly(xp,T(0),T(1)/4,T(1)/8,T(5)/64,T(7)/128,T(21)/512,T(33)/1024)
        else
            CC = (1-C)/(1+C)
        end
        return 1/A * (C - B + log(BB*CC)/2)
    elseif p < minimum(d)
        return zero(1/p)
    else
        return one(1/p)
    end
end

@quantile_newton DipoleDist
