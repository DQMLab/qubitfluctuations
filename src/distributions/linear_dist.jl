using Distributions
using Distributions: @distr_support
import Distributions: params, minimum, maximum, mode, pdf, cdf, quantile
# we model the distribution of TLF distances from a central TLS as a linear function
# this is valid for uniformly distributed TLFs in a puck-shaped volume
# since the cumulative density function is just the constant density multiplied by
# the volume (of a cylinder with radius r), the pdf (derivative of cdf) is ∝ r

struct LinearDist{T<:Real} <: ContinuousUnivariateDistribution
    r_min::T
    r_max::T
end

@distr_support LinearDist d.r_min d.r_max

params(d::LinearDist) = (d.r_min, d.r_max)

mode(d::LinearDist) = d.r_max

function pdf(d::LinearDist, r::Real)
    r_min, r_max = d.r_min, d.r_max
    p = 2*r/(r_max^2-r_min^2)
    if insupport(d, r)
        return p
    else
        return zero(p)
    end
end

function cdf(d::LinearDist, r::Real)
    r_min, r_max = d.r_min, d.r_max
    c = (r^2-r_min^2)/(r_max^2-r_min^2)
    if insupport(d, r)
        return c
    elseif r < minimum(d)
        return zero(c)
    else
        return one(c)
    end
end

function quantile(d::LinearDist, q::Real)
    r_min, r_max = d.r_min, d.r_max
    return √(q*r_max^2 + (1-q)*r_min^2)
end
