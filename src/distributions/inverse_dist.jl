using Distributions
using Distributions: @distr_support, @quantile_newton, quantile_newton,
    cquantile_newton, invlogcdf_newton, invlogccdf_newton
import Distributions: params, pdf, cdf, minimum, maximum, mode, quantile,
    cquantile, invlogcdf, invlogccdf
# we model the distribution of switching rates of thermally activated TLFs by an
# inverse distribution, as done in Faoro & Ioffe (2015). https://doi.org/10.1103/PhysRevB.91.014201

struct InverseDist{T<:Real} <: ContinuousUnivariateDistribution
    γ_min::T
    γ_max::T
end

@distr_support InverseDist d.γ_min d.γ_max

params(d::InverseDist) = (d.γ_min, d.γ_max)

mode(d::InverseDist) = d.γ_min

function pdf(d::InverseDist, γ::Real)
    if insupport(d, γ)
        γ_min, γ_max = d.γ_min, d.γ_max
        A = log(γ_max/γ_min)
        return 1/A * 1/γ
    else
        return zero(1/γ)
    end
end

function cdf(d::InverseDist, γ::Real)
    if insupport(d, γ)
        γ_min, γ_max = d.γ_min, d.γ_max
        A = log(γ_max/γ_min)
        return 1/A * log(γ/γ_min)
    elseif γ < minimum(d)
        return zero(1/γ)
    else
        return one(1/γ)
    end
end

function quantile(d::InverseDist, q::Real)
    γ_min, γ_max = d.γ_min, d.γ_max
    return quantile_inverse_dist(γ_min, γ_max, q)
end

quantile_inverse_dist(γ_min::Real, γ_max::Real, q::Real) = γ_min*(γ_max/γ_min)^q
