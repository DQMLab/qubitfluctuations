using Distributions
using Distributions: @distr_support
import Distributions: params, minimum, maximum, mode, pdf, cdf, quantile
# we model the distribution of TLF distances from a central TLS as a square function
# this is valid for uniformly distributed TLFs in 3D space
# since the cumulative density function is just the constant density multiplied by
# the volume (of a sphere with radius r), the pdf (derivative of cdf) is ∝ r²

struct SquaredDist{T<:Real} <: ContinuousUnivariateDistribution
    r_max::T
end

@distr_support SquaredDist zero(d.r_max) d.r_max

params(d::SquaredDist) = (d.r_max,)

mode(d::SquaredDist) = d.r_max

function pdf(d::SquaredDist, r::Real)
    r_max = d.r_max
    p = 3*r^2/r_max^3
    if insupport(d, r)
        return p
    else
        return zero(p)
    end
end

function cdf(d::SquaredDist, r::Real)
    r_max = d.r_max
    c = (r/r_max)^3
    if insupport(d, r)
        return c
    elseif r < minimum(d)
        return zero(c)
    else
        return one(c)
    end
end

function quantile(d::SquaredDist, q::Real)
    r_max = d.r_max
    return r_max*∛q
end
