module QubitFluctuations

using SpecialFunctions, LinearAlgebra
using LoopVectorization, Transducers
# we model T1 fluctuations by simulating a bunch of coupled TLS
# each TLS has an impact on the relaxation rate, which we know analytically
# to model fluctuations, we vary the coupling strength and frequency of these
# TLS, which affects their effect on T1

# the TLS are generated according to the simple method in the Xmon article

export DipoleDist, relax_rate, qubit_v_time, heatmap, gen_g_dist

include("distributions/dipole_dist.jl")
include("distributions/inverse_dist.jl")
include("distributions/gtm_dist.jl")
include("distributions/linear_dist.jl")
include("coupling.jl")
include("ts_gen.jl")
include("tls_fluctuations.jl")
include("relaxation.jl")
include("plotting.jl")

end # module
