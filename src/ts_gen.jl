using FFTW, DSP, Statistics

function random_noise_ts(tl, dt, mean_val, h₀, h₋₁, h₋₂, τ=Inf)
    # to generate a time series, we invert the spectrum with IFFT,
    # using random phases for each component
    Fs = 1/dt
    N = round(Int,tl*Fs) + 1
    # compute frequency array
    f = Fs/2 * range(0,1,length=N÷2+1)
    # evaluate PSD
    #rts_noise = (f,A,τ) -> 2*A^2*τ/(4+(2π*f*τ)^2)
    psd = h₀ .+ h₋₁./f .+ h₋₂./f.^2 # .+ rts_noise.(f,A,τ)
    psd[1] = (mean_val)^2 * N * dt * 2
    # rescale for IFFT assuming rectangular window
    psd .= psd .* (Fs * N / 2)
    # generate random phases
    ϕ = rand(length(psd)) .* 2π
    ϕ[1] = 0; ϕ[end] = 0 # d.c. & Nyquist components are real
    # compute complex spectrum
    S = sqrt.(psd) .* cis.(ϕ)
    # generate time series with real inverse FFT of PSD
    ts = irfft(S,N)
    if τ < Inf # add in random telegraph noise
        rts_noise!(ts,dt,τ,τ)
    end
    return ts
end

function rts_noise!(ts::Vector,dt::Number,τ1::Number,τ2::Number)
    # model an exponential RTS process
    # generate a random telegraph noise from a time series by setting the signal's value
    # to zero depending on the given relaxation time constant(s)
    # the probability of spending time t in a state with time constant τ is p(t) = exp(-t/τ)
    if dt < min(τ1,τ2) # for this method to work, the time step must be small
        state = rand(Bool)
        a = 1
        while a < length(ts)
            τ = state ? τ1 : τ2
            steps = round(Int, -log(rand())*τ/dt) # time spent in current state
            a == 1 && (steps = rand(0:steps)) # initially we could be at any point of process
            if !state
                b = min(a+steps, length(ts))
                ts[a:b] .= 0
            end
            state = state ⊻ true
            a = a + steps + 1
        end
    else
        # TODO
    end
    return ts
end

function add_rts_noise!(ts::Vector,dt::Number,τ1::Number,τ2::Number,δ::Number)
    # model an exponential RTS process
    # add random telegraph noise of stepsize 2δ to a time series by changing the signal's value
    # depending on the given relaxation time constant(s)
    # the probability of spending time t in a state with time constant τ is p(t) = exp(-t/τ)
    if dt < min(τ1,τ2) # for this method to work, the time step must be small
        state = rand(Bool)
        a = 1
        while a < length(ts)
            τ = state ? τ1 : τ2
            steps = round_clamp(Int, -log(rand())*τ/dt) # time spent in current state
            a == 1 && (steps = rand(0:steps)) # initially we could be at any point of process
            b = min(a+steps, length(ts))
            if state
                ts[a:b] .+= δ
            else
                ts[a:b] .-= δ
            end
            state = state ⊻ true
            a = a + steps + 1
        end
    else # assume switching averages out to 0
        #ts .+= (rand(Bool,length(ts)).*2 .- 1) .* δ
    end
    return ts
end

round_clamp(::Type{T}, x) where {T} = x > typemax(T) ? typemax(T) : round(T, x)