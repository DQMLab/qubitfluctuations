function cpw_field(x::Real, y::Real, a::Real, b::Real)
    # compute E-field at position (x,y) along the cross section of a CPW
    # field is normalized by potential (or equivalently ϕ₀ = 1)
    z = complex(x,y)
    E = b/ellipk(1-(a/b)^2)/√((z^2-a^2)*(z^2-b^2))
    if x^2 > y^2 + (a^2+b^2)/2
        E = -E
    end
    return copysign(imag(E),x), real(E)
end

function rand_field_mag(N::Integer, w_cs::Real, t_oxide::Real)
    # draw N random positions on the CPW cross-section and return the magnitude of
    # the field there, scaled to a potential of ϕ₀ = (Ej/2Ec)^(1/4)*𝑒/C = 4μV
    ϕ₀ = 4.0 # μV
    a = 12.0 # μm
    b = 36.0 # μm
    # draw positions along cross-section
    x_pos = -w_cs/2 .+ w_cs.*rand(N)
    y_pos = t_oxide.*rand(N)
    # calc E-field magnitude (V/m)
    return norm.(cpw_field.(x_pos,y_pos,a,b)).*ϕ₀
end

function gen_g_dist(d::DipoleDist, BW::Real=1, min_g::Real=0)
    # return a distribution of TLS coupling strength g in MHz
    # for a transmon with the geometry defined below
    # the bandwidth BW must be given in GHz and will determine
    # the number of values returned

    ρ0 = 200 # (GHz*μm³)⁻¹, high-frequency TLS density
    n_arms = 4 # for our Xmon
    l_arm = 188 # approximate due to ignoring the middle section of the cross and the end gap
    w_cs = 96 # μm, width of the cross-section we draw from
    t_oxide = 3e-3 # 3nm oxide thickness

    # calc number of TLSs in volume for given bandwidth
    spectral_density = ρ0*n_arms*l_arm*w_cs*t_oxide # per GHz
    nTLS = round(Int, BW*spectral_density) # multiply by BW

    # draw dipole moments (Debye), multiply by E (V/m) to get g, then convert to MHz
    k = 5034.116567542709302e-6 # 1D/ℎ in MHz * m/V
    g = rand(d,nTLS) .* rand_field_mag(nTLS,w_cs,t_oxide) .* k # MHz

    # sort g values
    sort!(g, rev=true)

    # apply cutoff if needed
    if min_g > 0
        m = findfirst(g->g<min_g, g)
        if m !== nothing
            g = g[1:(m-1)]
        end
    end
    return g
end


# legacy function, do not use
function gen_g_dist_simple(d::DipoleDist, N::Integer=400, max_g::Real = 0.33)
    # figure out mean E field according to g = d*E (assuming mean E value)
    E_max = max_g/d.p_max # units depend on g, in V/m if g is in energy units
    # make field normally distributed because we don't have sims
    dist_E = Normal(2/3*E_max,1/3*E_max/3) # max val is 3 sigma away
    # draw dipole moments and multiply by E to get g
    g = rand(d, N) .* rand(dist_E, N) # same units as max_g
end