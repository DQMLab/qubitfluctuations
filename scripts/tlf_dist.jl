using QubitFluctuations, Distributions, Random, PyPlot, BenchmarkTools
using QubitFluctuations: GTMDist, tls_tls_shift
PyPlot.close_figs()
const figpath = joinpath(@__DIR__,"figs")


Dγ = InverseDist(1E-6,1E-1)
γ = 10 .^ range(-6,-1,length=101)
plot(γ, pdf.((Dγ,),γ))

Δ₀ = rand(DΔ,10^6)[2,:]
V = @. K*log(Ω₀/Δ₀)^2
γ = @. exp(-V/200) # 10mK ∼ 200MHz
minimum(γ)
count(γ->γ>1.5E-2,γ)
hist(rand(Dγ,10^6),bins=0:1E-6:4E-3)
xscale("log")
yscale("log")
#xlim([1E-6,1E-3])


Δ_min, Δ_max, μ = 5E0, 10E3, 0.3
DΔ = GTMDist(Δ_min,Δ_max,μ)

## Draw TLFs
N_DRAWS = 20*10^6
Δ, Δ₀ = eachrow(rand(DΔ,N_DRAWS))
E = sort(@. √(Δ^2+Δ₀^2))

f = figure(1,figsize=(5,4)); #f.clf();
hist(E,bins=0:100:Δ_max)
xlabel("TLS Energy (MHz)");
ylabel("Number of TLS in 100MHz BW");
grid(true); grid(which="minor",ls="--",alpha=0.25); display(f);

f = figure(2,figsize=(5,4)); f.clf();
plot(E, cumsum(ones(length(E))));
xlim(0,Δ_max); ylim(0,N_DRAWS);
xlabel("TLS Energy (MHz)");
ylabel("Cumulative Number of TLS");
grid(true); grid(which="minor",ls="--",alpha=0.25); display(f);


## TLFs are draw from the GTM distribution, which is roughly uniform in Δ and inverse in Δ₀
x = LinRange(0,QubitFluctuations.Δ_max,501)
y = LinRange(QubitFluctuations.Δ_min,QubitFluctuations.Δ_max/50,501)
f = (x,y) -> pdf(QubitFluctuations.DΔ,[x,y])
figure(4,figsize=(9,6)); clf()
heatmap(x, y, permutedims(f.(x,permutedims(y))),
    xlabel = "Δ (MHz)", ylabel = "Δ₀ (MHz)", clabel="pdf(Δ,Δ₀)");
savefig(joinpath(figsdir,"gtm_distribution.png"), dpi=600, transparent=true);