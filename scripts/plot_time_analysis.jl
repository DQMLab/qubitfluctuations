using PyPlot, PGFPlotsX
PyPlot.close_figs()

function plot_time_analysis(Γ1,dt,tl; save_pyplot="", save_pgfplots="")
    l_seg = (100*3600)÷dt # 100-hour segment length for averaging
    noverlap = 0
    
    fig = figure(18,figsize=(10,10)); fig.clf()
    (ax1,ax2,ax3,ax4) = fig.subplots(4,1);
    ax1.plot((0:dt:tl)./3600, inv.(Γ1),".");
    ax1.set(xlabel="Time (h)", ylabel="T₁ (μs)", xlim=(0,100), ylim=(0,80)); ax1.grid(true);
    @pgf pgf_ax1 = Axis({xlabel="Time (h)", ylabel="\$T_1\$ (\\textmu s)", grid="major"},
    PlotInc({thick,x="t",y="T1"},Table([:t => (0:dt:tl)./3600, :T1 => inv.(Γ1)])))
    
    # PSD
    P = periodogram(inv.(vec(Γ1)), fs=1/dt)
    ax2.plot(P.freq[2:end],P.power[2:end],label="$(round(Int,tl/3600)) hours, unaveraged")
    ax2.set(xlabel="Frequency (Hz)", ylabel="Power Spectral Density (μs²/Hz)", xscale="log", yscale="log");
    ax2.grid(true); ax2.grid(which="minor",ls="--",alpha=0.25);
    @pgf pgf_ax2 = LogLogAxis({xlabel="Frequency (Hz)", ylabel="PSD (\\textmu s\\textsuperscript{2}/Hz)", grid="both", legend_columns=-1, legend_entries=["No averaging","100 hours","200 hours","500 hours","1000 hours"], legend_pos="south west"},
    PlotInc({thick,x="f",y="S"},Table([:f => P.freq[2:end], :S => P.power[2:end]])))
    
    # Overlapping Allan deviation
    tau, adev = allandev(inv.(vec(Γ1)), 1/dt, overlapping=true, frequency=true, taus=100)
    ax3.plot(tau,adev,label="$(round(Int,tl/3600)) hours, unaveraged")
    ax3.set(xlabel="τ (s)", ylabel="Overlapping Allan Deviation (μs)", xscale="log", yscale="log", xlim=(10^2,10^5), ylim=(1,7));
    ax3.grid(true); ax3.grid(which="minor",ls="--",alpha=0.25);
    @pgf pgf_ax3 = LogLogAxis({xlabel="\$\\tau\$ (s)", ylabel="Overlapped \$\\sigma_T_1\$ (\\textmu s)", grid="both"},
    PlotInc({thick,x="tau",y="adev"},Table([:tau => tau, :adev => adev])))
    
    # Allan deviation
    tau, adev = allandev(inv.(vec(Γ1)), 1/dt, overlapping=false, frequency=true, taus=100)
    ax4.plot(tau,adev,label="$(round(Int,tl/3600)) hours, unaveraged")
    ax4.set(xlabel="τ (s)", ylabel="Allan Deviation (μs)", xscale="log", yscale="log", xlim=(10^2,10^5), ylim=(1,7));
    ax4.grid(true); ax4.grid(which="minor",ls="--",alpha=0.25);
    @pgf pgf_ax4 = LogLogAxis({xlabel="\$\\tau\$ (s)", ylabel="\$\\sigma_T_1\$ (\\textmu s)", grid="both"},
    PlotInc({thick,x="tau",y="adev"},Table([:tau => tau, :adev => adev])))
    
    for n in [1000]*(3600÷dt)
        local P, tau, adev
        # PSD
        P = welch_pgram(inv.(Γ1[1:n]), l_seg, noverlap, fs=1/dt)
        ax2.plot(P.freq[2:end],P.power[2:end],label="$(round(Int,n*dt/3600)) hours")
        push!(pgf_ax2, @pgf PlotInc({thick,x="f",y="S"},Table([:f => P.freq[2:end], :S => P.power[2:end]])))
        
        # Overlapping Allan deviation
        tau, adev = allandev(inv.(Γ1[1:l_seg]), 1/dt, overlapping=true, frequency=true, taus=100)
        adev .= adev.^2 # temporarilly convert to variance for averaging
        for i in 2:n÷l_seg
            data = inv.(Γ1[(i-1)*l_seg+1:i*l_seg])
            adev .+= (allandev(data, 1/dt, overlapping=true, frequency=true, taus=100)[2]).^2
        end
        adev .= sqrt.(adev./(n÷l_seg))
        # fit
        params, errors = QuAnalysis.fit_allan_deviation(tau,adev,0.8E4)
        display([params errors])
        adev_fit = similar(adev)
        QuAnalysis.modelAllanDev1!(adev_fit, tau, params)
        #
        ax3.plot(tau,adev,label="$(round(Int,n*dt/3600)) hours, 1/τ₀ = $(round(Int,1E6/params[2])) μHz")
        ax3.plot(tau,adev_fit,"--",color=ax3.lines[end].get_color())
        push!(pgf_ax3, @pgf PlotInc({thick,x="tau",y="adev"},Table([:tau => tau, :adev => adev])))
        
        # Allan deviation
        tau, adev = allandev(inv.(Γ1[1:l_seg]), 1/dt, overlapping=false, frequency=true, taus=100)
        adev .= adev.^2
        for i in 2:n÷l_seg
            data = inv.(Γ1[(i-1)*l_seg+1:i*l_seg])
            adev .+= (allandev(data, 1/dt, overlapping=false, frequency=true, taus=100)[2]).^2
        end
        adev .= sqrt.(adev./(n÷l_seg))
        ax4.plot(tau,adev,label="$(round(Int,n*dt/3600)) hours")
        push!(pgf_ax4, @pgf PlotInc({thick,x="tau",y="adev"},Table([:tau => tau, :adev => adev])))
    end
    foreach(ax->ax.legend(), (ax2,ax3,ax4))
    fig.set_tight_layout(true); display(fig);
    
    if save_pyplot != ""
        fig.savefig(joinpath(@__DIR__,"figs",save_pyplot), dpi=600, facecolor="#fbfbfb")
    end
    
    
    if save_pgfplots != ""
        ## save pfgplot
        @pgf gp = GroupPlot(
        {group_style={group_size = "1 by 4", vertical_sep="1.5cm"}, height="3.67cm", cycle_list_name="matlab", minor_grid_style={"densely dashed", "very thin","black!10"}},
        pgf_ax1, pgf_ax2, pgf_ax3, pgf_ax4)
        pgfsave(joinpath(@__DIR__,"figs","increasing_time_analysis.tikz"),gp)
    end
end