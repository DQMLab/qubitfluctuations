using PyPlot

# Determine volume where the E-field is strong based on fig S5 (b)
# we assume an oxide volume composed of
# ground plane, 4 * 100nm long sections * 2 sides
# center cond, 7 * 100nm long sections * 2 sides
# each section has thickness 3nm
# each arm is 165μm long, and there are 4 arms
# so total vol is (4*2 + 7*2) * 100E-9 * 3E-9 * 165E-6 * 4
V = 4.36 # μm^3

x = [-12.2,-12.1,-12.0,-4.1,-4.0,-3.9,-3.8,-3.7,-3.6,3.7,3.8,3.9,4.0,4.1,11.9,12.0,12.1]
y = [2,5,22,7,40,9,9,4,3,1,9,14,47,8,1,24,5]
bar(x,y,0.1,edgecolor="black",linewidth=0.5); grid(true); xticks(-12:4:12);
