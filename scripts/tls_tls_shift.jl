using QubitFluctuations, Random, PyPlot, BenchmarkTools
using QubitFluctuations: DipoleDist, relax_rate, relax_rate_v_time, heatmap, tls_tls_shift

const DIR = @__DIR__

E = 0 : 0.01 : 50
d = 0 : 0.01 : 1 # d = Δ/E
U = 2;

f = (U, E, d) -> tls_tls_shift(U, E, d*E)

for U in [0.1, 1, 10]
figure(figsize=(9,6));
heatmap(E, d, permutedims(f.(U, E, permutedims(d))),
    xlabel = "E (MHz)", ylabel = "Δ/E", clabel="δf (MHz)");
title("U = $U (MHz)");
end
savefig(joinpath(DIR,"tls_tls_shift.png"), dpi=600, transparent=true);
