using QubitFluctuations, QuAnalysis, Statistics, Dates, DSP, AllanDeviations
include(joinpath(@__DIR__,"plot_time_analysis.jl"))

# simulation params
T1_max = 60 # μs
Γ_qubit = 1/(T1_max-3) # MHz
min_d = 0.1 # Debye
max_d = 6.0 # Debye

# coupling strengths
d = DipoleDist(min_d,max_d)
g = gen_g_dist(d, 1, 0.007)

# qubit at single freq
fq = 4500.0
# TLS freq (around qubit)
f_TLS = 4000 .+ 1000 .* rand(N_cTLS)
idx = findall(f->abs(f-fq)<50, f_TLS) # only keep TLSs within 50MHz

# experimental parameters
dt = 100 # 100s sampling period
tl = 1000*3600 # 1000 hours or ~42 days

# simulate experiment with known fluctuators
Γ_TLS_max = 100.0
Γ_TLS = rand(QubitFluctuations.InverseDist(0.2,1.0),length(idx)).^2 .* Γ_TLS_max
tlf_params = Dict{Float64,Vector{Tuple{Float64,Float64}}}()
f_TLS_many_ts = reduce(hcat, [QubitFluctuations.tls_freq_v_time(fTLS,tl,dt,tlf_params) for fTLS in f_TLS[idx]])
Γ1, _ = qubit_v_time(tl, dt, f_TLS_many_ts, g[idx], Γ_TLS, [fq], Γ_qubit)

plot_time_analysis(Γ1,dt,tl)


unzip(X::Vector{<:NTuple{N}}) where {N} = ntuple(n->getindex.(X,n), N)

f_TLS_many_ts_2 = reduce(hcat, [QubitFluctuations.tls_freq_v_time(fTLS,tl,dt,unzip(tlf_params[fTLS])...) for fTLS in f_TLS[idx]])
Γ1, _ = qubit_v_time(tl, dt, f_TLS_many_ts_2, g[idx], Γ_TLS, [fq], Γ_qubit)

plot_time_analysis(Γ1,dt,tl)



# simulate a single TLS with a single fluctuator
f_TLS_single = 4501.1
γ = [200E-6]
δf = [0.6]
f_TLS_single_tls = hcat(QubitFluctuations.tls_freq_v_time(f_TLS_single, tl, dt, γ./2, δf))
Γ1, _ = qubit_v_time(tl, dt, f_TLS_single_tls, [0.04], [15.0], [fq], Γ_qubit)

plot_time_analysis(Γ1,dt,tl,save_pyplot="allan_analysis_1_tls.png")

# simulate 4 TLS, with a single fluctuator each
f_TLS = [4501.1, 4501.5, 4498.9, 4998.6]
g = [0.02, 0.02, 0.02, 0.02]
Γ_TLS = [10, 10, 10, 10]
γ  = [[200], [200], [200], [200]] .* 1e-6
δf = [[0.8], [0.8], [0.8], [0.8]]
f_TLS_4_tls = reduce(hcat, [QubitFluctuations.tls_freq_v_time(f_TLS[i],tl,dt,γ[i]./2,δf[i]) for i in 1:4])
Γ1, _ = qubit_v_time(tl, dt, f_TLS_4_tls, g, Γ_TLS, [fq], Γ_qubit)

plot_time_analysis(Γ1,dt,tl,save_pyplot="allan_analysis_4_tls")

function find_fluctuators_around(γ, tlf_params)
    results = zeros(0,4)
    for (fTLS,TLFs) in tlf_params
        x,idx = findmin(abs.(log10.(first.(TLFs)./γ)))
        if x < 0.301 # at least within a factor of 2
            results = vcat(results, hcat(x, TLFs[idx]..., fTLS))
        end
    end
    return sortslices(results, dims=1, by=first) # sort according to x
end