using CircuitQED, Schrodinger, PyPlot, SparseArrays, LinearAlgebra

Cj = 100E-15
Lj = 8E-9
Cc = 0.431E-15
ωTLS = 5E9 * 2π
Cm = [0.0 -Cc; -Cc 0.0]
elems = (Xmon(Lj,Cj,3,0.0),Resonator(ωTLS,2))
c = Circuit(elems,Cm)


fluxes = 0 : 0.001 : 0.3
shifts = Matrix{Float64}(undef,length(fluxes),3)
for (i,z) = enumerate(fluxes)
    zbias!(c.elements[1],z)
    H = dense(hamiltonian(c))
    g = H[(0,1),(1,0)]
    ωq = CircuitQED.ω(c,1)
    α = CircuitQED.α(c,1)

    D,V = eigen(H / (2π*1E6))
    f_0 = D[findstate(V,(1,0))]-D[findstate(V,(0,0))]
    f_1 = D[findstate(V,(1,1))]-D[findstate(V,(0,1))]
    #shifts[i,1] = (f_1 - f_0)/2 # CircuitQED
    shifts[i,1] = f_0 -  ωq/(2π*1E6)

    ####### Approx

    H = (ωq*numberop(3) + α*projectorop(3,2))⊗qeye(2) + g*(create(3)+destroy(3))⊗(create(2)+destroy(2)) + ωTLS*qeye(3)⊗numberop(2)
    D,V = eigen(dense(H) / (2π*1E6))
    f_0 = D[findstate(V,(1,0))]-D[findstate(V,(0,0))]
    f_1 = D[findstate(V,(1,1))]-D[findstate(V,(0,1))]
    #shifts[i,2] = (f_1 - f_0)/2 # Anharmonic three-level system
    shifts[i,2] = f_0 -  ωq/(2π*1E6)

    ###### Blais-ish approximation
    Δ = ωTLS-ωq;
    f_0 = (ωq -     sign(Δ)/2*√(  4g^2+Δ^2) + Δ/2)/(2π*1E6)
    f_1 = (ωq+α/2 + sign(Δ)/2*√(2*4g^2+(Δ-α)^2) - sign(Δ)/2*√(4g^2+(Δ)^2))/(2π*1E6)
    #shifts[i,3] = (sign(Δ-α)*√(8g^2+(Δ-α)^2)-(Δ-α))/4 / (2π*1E6) # Analytic approx
    shifts[i,3] = f_0 -  ωq/(2π*1E6)
end

clf()
plot(fluxes,shifts)
display(gcf())
