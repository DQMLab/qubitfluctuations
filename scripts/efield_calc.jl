# Calculating E-field values analytically for an Xmon
# See 10.1109/TMTT.2018.2841829, and in particular Eq. 32 & 33
using SpecialFunctions, Cubature, PyPlot, PyCall
using QubitFluctuations: heatmap
using ForwardDiff: derivative
const inset_locator = PyPlot.LazyPyModule("mpl_toolkits.axes_grid1.inset_locator")
const tpl = pyimport("tikzplotlib")
const figsdir = joinpath(@__DIR__,"figs")


## setup

function cpw_field(x,y,a,b)
    # field is normalized by potential (or equivalently ϕ₀ = 1)
    z = complex(x,y)
    E = b/ellipk(1-(a/b)^2)/√((z^2-a^2)*(z^2-b^2))
    if x^2 > y^2 + (a^2+b^2)/2
        E = -E
    end
    return copysign(imag(E),x), real(E)
end

a = 12.0
b = 36.0
x = 0 : 1/5 : 40
y = 0 : 1/5 : 10
T = cpw_field.(transpose(x),y,a,b)
U,V = getindex.(T,1), getindex.(T,2)
C = norm.(T)

## integrate along line to verify potential
s = 0 : 0.002 : 1
ellipse_x(s,x₀,y₀,a,b) = (2s-1)*a+x₀
ellipse_y(s,x₀,y₀,a,b) = √(1-((ellipse_x(s,x₀,y₀,a,b)-x₀)/a)^2)*b + y₀
el_a = 14
el_b = 3
el_x₀ = 24
function path_int(s,a,b,el_a,el_b,el_x₀)
    x = ellipse_x(s,el_x₀,0,el_a,el_b)
    y = ellipse_y(s,el_x₀,0,el_a,el_b)
    Ex,Ey = cpw_field(x,y,a,b)
    return (Ex * 2*el_a) + (Ey * derivative(s->ellipse_y(s,el_x₀,0,el_a,el_b), s))
end
@show hquadrature(s->path_int(s,a,b,el_a,el_b,el_x₀), eps(), 1-eps()) # should be ~1

## vector field
PyPlot.close_figs()
fig1 = figure(num=1,figsize=(10,4)); fig1.clf();
vlines([-b,-a,a,b],ymin=0,ymax=10,color="gray",ls="--",linewidth=1,zorder=1);
hlines([0,0,0],[minimum(x),-a,b],[-b,a,maximum(x)],color="red",linewidth=4,zorder=1);
quiver(x,y,U,V,C,norm=matplotlib.colors.LogNorm(1e-2,0.2),cmap="viridis",units="dots",pivot="mid",scale=3e-2,headlength=4,headaxislength=4,width=0.5);
#plot(ellipse_x.(s,el_x₀,0,el_a,el_b),ellipse_y.(s,el_x₀,0,el_a,el_b));
axis("image"); xlim(extrema(x)); ylim(extrema(y)); tight_layout();
cbaxes = inset_locator.inset_axes(gca(), width="1%", height="90%", loc=6)
colorbar(cax=cbaxes); cbaxes.set(ylabel="Normalized E-field E/ϕ₀ (μm⁻¹)"); display(fig1);
savefig(joinpath(figsdir,"cpw_efield.pdf"));

## magnitude 0.5, 1, and 2nm away
log10x = exp10.(range(-3, log10(12), length=128))
x = [reverse(12 .- log10x); 12; 12 .+ log10x; reverse(36 .- log10x); 36; 36 .+ log10x]
T0 = norm.(cpw_field.(x,0.5e-3,a,b))
T1 = norm.(cpw_field.(x,1.0e-3,a,b))
T2 = norm.(cpw_field.(x,2.0e-3,a,b))
T15 = norm.(cpw_field.(x,1.5e-3,a,b))

fig2 = figure(num=2,figsize=(8,4)); fig2.clf();
vlines([-b,-a,a,b],ymin=1e-2,ymax=10,color="gray",ls="--",linewidth=1,zorder=1);
hlines([1e-2,1e-2,1e-2],[minimum(x),-a,b],[-b,a,maximum(x)],color="red",linewidth=4,zorder=1);
semilogy(x,T15); #legend(["0.5 nm","1.0 nm","2.0 nm"],loc=2);
xlabel("Position (μm)")
ylabel("Normalized E-field mag. |E|/ϕ₀ (μm⁻¹)");
xlim(0,maximum(x)); ylim(1e-2,10); tight_layout(); display(fig2);
savefig(joinpath(figsdir,"cpw_efield_mag.pdf"));
tpl.save(joinpath(figsdir,"cpw_efield_mag.tikz"),figure=fig2,include_disclaimer=false,encoding="utf-8")

## as a heatmap
y = 0 : 1e-5 : 2e-3
T = norm.(cpw_field.(permutedims(x),y,a,b))

fig3 = figure(num=3,figsize=(8,4)); fig3.clf();
hlines([0,0,0],[minimum(x),-a,b],[-b,a,maximum(x)],color="red",linewidth=4,zorder=1);
heatmap(x,y,T; lim=(1e-2,10), ar=1.9,clabel="Normalized E-field mag. |E|/ϕ₀ (μm⁻¹)", cnorm=:lognorm);
ticklabel_format(axis="y",style="sci",scilimits=(-1,1),useMathText=true);
xlim(0,maximum(x)); ylim(0,maximum(y)); display(fig3);
savefig(joinpath(figsdir,"cpw_efield_mag_map.pdf"),dpi=1000);

# Max value at 1nm is ~2.5/μm, if the potential is 4μV, we have 10V/m E-field maximum
# this is consistent with the Barends article

# To draw TLSs, we assume that the Xmon E-field can be well described by that of the cross-section
# of a CPW. The total volume determines how many TLSs we pick.
# Note that with these simulations, we assume that the metal is a flat sheet, and so ignore the
# "walls". We also ignore the fact that the geometry is not a CPW everywhere.

ρ0 = 100 # (GHz*μm^3)⁻¹, high-frequency TLS density

n_arms = 4
l_arm = 188 # approximate due to ignoring the middle section of the cross and the end gap
t_oxide = 2e-3 # 2nm oxide thickness
w_cs = 96 # width of the cross-section we draw from

# number of TLSs to draw
nTLS = round(Int, ρ0 * n_arms*l_arm*t_oxide*w_cs) # per GHz

# draw positions along cross-section
x_pos = -w_cs/2 .+ w_cs.*rand(nTLS)
y_pos = t_oxide.*rand(nTLS)

# calc E-field
T = norm.(cpw_field.(x_pos,y_pos,a,b))

## plot TLS pos and E-field mag as color
fig4 = figure(num=3,figsize=(8,4)); fig4.clf();
vlines([-b,-a,a,b],ymin=0,ymax=t_oxide,color="gray",ls="--",linewidth=1,zorder=1);
hlines([0,0,0],[minimum(x_pos),-a,b],[-b,a,maximum(x_pos)],color="red",linewidth=4,zorder=1);
scatter(x_pos[1:nTLS÷4],y_pos[1:nTLS÷4],c=T[1:nTLS÷4],s=5.0,norm=matplotlib.colors.LogNorm(1e-2,10));
colorbar().set_label("Normalized E-field mag. |E|/ϕ₀ (μm⁻¹)")
ticklabel_format(axis="y",style="sci",scilimits=(0,0),useMathText=true);
xlim(0,w_cs/2); ylim(0,t_oxide); tight_layout(); display(fig4);
savefig(joinpath(figsdir,"cpw_efield_tls.pdf"));

## histogram
fig5 = figure(num=4,figsize=(8,4)); fig5.clf();
hist(T,bins=exp10.(-2:0.05:1)); legend(["$nTLS TLSs in total"]);
xlabel("Normalized E-field mag. |E|/ϕ₀ (μm⁻¹)");
xlim(0.01,10); xscale("log"); tight_layout(); display(fig5);
savefig(joinpath(figsdir,"cpw_efield_tls_hist.pdf"));

k = 5034.116567542709302e-6 # 1D/ℎ in MHz * m/V
maximum(T)*4.0 * k # MHz, for 1D dipole moment
