using QubitFluctuations, Distributions, Random, PyPlot, BenchmarkTools
using QubitFluctuations: GTMDist, tls_tls_shift
PyPlot.close_figs()
const figpath = joinpath(@__DIR__,"figs")

Δ_min, Δ_max, μ = 125E0, 1E3, 0.3
Ω₀ = 1E3 # Well frequency, 2THz in Phillip's review, must be >= Δ_max
K = 8E3 # K = 1/2m*(ħ/d)², 8GHz for m = 16u, d = 2Å
T = 1250 # 60mK in MHz (kB*T/ℎ)
E_max = T/2 # maximum energy allowed for a thermally activated fluctuator

# At the very least, we need Ω₀ > E_max
@assert Ω₀ >= min(E_max,Δ_max)
# We also want V_min >= Δ_max (in order to have a TLS)
V_min = K*log(Ω₀/min(E_max,Δ_max))^2
@assert V_min >= min(E_max,Δ_max)

# γ = γ₀*exp(-K/T*log(Ω₀/Δ₀)^2)
γ_max = 1E-1 # switching rate at maximum Δ₀ = E_max
switching_rate(Δ₀,T,K,Ω₀,log_γ₀,Δ=0) = exp(-(K*log(Ω₀/Δ₀)^2+Δ)/T+log_γ₀)
log_γ₀ = K/T*log(Ω₀/E_max)^2 + log(γ_max)

γ₀ = exp(log_γ₀)
γ_min = switching_rate(Δ_min,T,K,Ω₀,log_γ₀)
@assert switching_rate(E_max,T,K,Ω₀,log_γ₀) ≈ γ_max


## Draw TLFs
DΔ = GTMDist(Δ_min,Δ_max,μ)
N_DRAWS = 10^5
Δ, Δ₀ = eachrow(rand(DΔ,N_DRAWS))
E = @. √(Δ^2+Δ₀^2)
U = QubitFluctuations.U₀./rand(QubitFluctuations.Dr,length(Δ)).^3
filt = @. √(E^2 + 4U*(Δ+U)) < E_max # keep TLFs that are thermally activated
E, Δ, Δ₀, U = E[filt], Δ[filt], Δ₀[filt], U[filt]
i = sortperm(Δ₀) # sort by Δ₀, slowest to fastest, highest barrier to smallest
E, Δ, Δ₀, U = E[i], Δ[i], Δ₀[i], U[i]

N_TLF = length(E)
println("Δ_min = $(DΔ.Δ_min), Δ_max = $(DΔ.Δ_max), Ω₀ = $Ω₀, K = $K")
println("Thermally activated TLFs: $N_TLF ($(100*N_TLF/N_DRAWS)%)")


## Plot figures for barrier height & switching rate
V_height = @. K*log(Ω₀/Δ₀)^2
f=figure(num=1,figsize=(9,6)); #f.clf();
plot(sort(V_height),(1:length(V_height))./N_TLF);
axvline(T,0,1, color="red"); annotate("T = $T", xy=(T*1.05,0.5));
xlabel("Barrier Height (MHz)"); ylabel("Cumulative Density");
grid(true); xlim(left=0); ylim(0,1); tight_layout();

γ = @. exp(-(V_height+Δ)/T+log_γ₀)
f=figure(num=2,figsize=(9,6)); #f.clf();
plot(sort(γ),(1:length(γ))./N_TLF)
#xlim([1E-8,1]) # 1E-8 corresponds to a switch rate of once each 3.2 years
xlabel("Switching Rate (Hz)"); ylabel("Cumulative Density");
grid(true); xscale("log"); xlim(1E-10,1E0); ylim(0,1); tight_layout();
#savefig(joinpath(figpath,"switching_rate_dist.png"), dpi=600, transparent=true);

# Try to figure out good values
i = findfirst(γ->γ>1E-7,γ)
j = findfirst(γ->γ>1E-4,γ)
x = 1-j/N_TLF; y = (j-i)/N_TLF
println("Density over 1E-4 Hz: $(round(100*x,digits=1))%, count: $(N_TLF-j)")
println("Density between 1E-7 and 1E-4 Hz: $(round(100*y,digits=1))%, count: $(j-i)")
println("Ratio: $(y/x)")

Δ₀_plot = range(0, min(Ω₀,T), length = 201)
f=figure(num=3,figsize=(9,6)); #f.clf();
plot(Δ₀_plot, switching_rate.(Δ₀_plot,T,K,Ω₀,log_γ₀));
axvline(T/2,0,1, color="red"); annotate("T/2 = $(T/2)", xy=(T/2*1.05,5*1E-12));
axhline(γ_max,0,1, color="green"); annotate("γ = γ_max", xy=(10,2*γ_max));
xlabel("Δ₀ (MHz)"); ylabel("γ (Hz)"); xlim(0,min(Ω₀,T)); ylim(1E-12,1E2);
grid(true); yscale("log"); tight_layout();


## Switching rate vs δf


δf = tls_tls_shift.(U,E,Δ)
#PyPlot.plt.style.use("dark_background");
f=figure(num=4,figsize=(10,8)); f.clf()
hb = hexbin(γ, δf, gridsize=80, vmax=30, cmap="inferno", xscale="log", yscale="log", extent=(-10,-2,-2,2), linewidths=0.2);
xlabel("Switching Rate (Hz)"); ylabel("Freq. Shift (MHz)");
cb = colorbar(hb); cb.set_label("counts");
ylim(1E-2,1E2); xlim(1E-10,γ_max); tight_layout();
#savefig(joinpath(figpath,"shift_vs_switching_rate.pdf"));


shift_vs_switching_rate(γ,U,Δ,T,K,Ω₀,log_γ₀) = (Δ₀=Ω₀*exp(-√((T*(log_γ₀-log(γ))-0Δ)/K)); tls_tls_shift(U,√(Δ^2+Δ₀^2),Δ))

γ_range = exp10.(range(log10(1E-10),log10(γ_max),length=200))
Δs = 0:100:E_max
U_const = 5
funs = reduce(hcat, [shift_vs_switching_rate.(γ_range,U_const,Δ,T,K,Ω₀,log_γ₀) for Δ in Δs])
plot(γ_range, funs); 
legend(string.("Δ = ", Δs, " MHz, U = ", U_const, " MHz"), loc="upper left");
ylim(0,10); xlim(1E-10,γ_max); grid(); tight_layout();


