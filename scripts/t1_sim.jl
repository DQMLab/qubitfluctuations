using QubitFluctuations, Random, PyPlot, PyCall
using QubitFluctuations.Distributions
@isdefined(tpl) || (const tpl = pyimport("tikzplotlib"))
@isdefined(figsdir) || (const figsdir = joinpath(@__DIR__,"figs"))


## simulation params
bw = 1.0 # BW in GHz
T1_max = 30 # μs
Γ_qubit = 1/(T1_max-3) # MHz
min_d = 0.1 # Debye
max_d = 6.0 # Debye
d = DipoleDist(min_d,max_d)


## plot moment distribution
p = range(d.p_min,d.p_max,length=2001)
figure(1,figsize=(5,4)); clf()
plot(p,pdf.(d,p));
plot(p,cdf.(d,p));
xlabel("Dipole Moment p (Debye)"); legend(["pdf","cdf"]);
#title("PDF & CDF of dipole distribution");
xlim(0,6); ylim(0,3); grid(true); tight_layout();
savefig(joinpath(figsdir,"dipole.png"), dpi=600, transparent=true)
#tpl.save(joinpath(figsdir,"dipole.tikz"),figure=figure(1),include_disclaimer=false,encoding="utf-8")


## draw coupling strengths
g = sort!(gen_g_dist(d,bw,0.007))
println("Above 0.01MHz: ",count(>(0.01), g))
println("Above 0.10MHz: ",count(>(0.1), g))

# plot (filter only above 0.01MHz)
figure(2,figsize=(5,4));
plot(filter(>(0.01), g), 1:count(>(0.01),g));
xlim(0.01,1); ylim(0,300);
xlabel("Coupling strength g/2π (MHz)");
ylabel("Cumulative Number of TLS with g/2π > 0.01MHz (1/GHz)");
grid(true); xscale("log"); grid(which="minor",ls="--",alpha=0.25);
savefig(joinpath(figsdir,"couplings.png"), dpi=600, transparent=true)
#tpl.save(joinpath(figsdir,"couplings.tikz"),figure=figure(2),include_disclaimer=false,encoding="utf-8")


## qubit freqs
f_qubit_all = 4250 : 2 :  4750

# TLS freq (around qubit)
f_TLS = 4000 .+ (bw*1000) .* rand(length(g))
Γ_TLS_max = 100.0
Γ_TLS = rand(QubitFluctuations.InverseDist(0.2,1.0),length(g)).^2 .* Γ_TLS_max


## compute relaxation rates for each TLS and add them up
swapspec = vec(sum(relax_rate.(f_TLS, g, transpose(f_qubit_all), Γ_qubit, Γ_TLS), dims=1)) .+ Γ_qubit
figure(3,figsize=(6,4));
plot(f_qubit_all.*1E-3,swapspec);
xlabel("Qubit Frequency (GHz)"); ylabel("Γ₁ (MHz)");
ylim(0,0.25); grid(true); tight_layout();
savefig(joinpath(figsdir,"swapspec.png"), dpi=600, transparent=true);


## look at time fluctuations
tl = 50*3600
dt = 5*60
qubit_v_time(tl, dt, rand(f_TLS), rand(g), 5000, Γ_qubit, 12.2, showplot=true);
savefig(joinpath(figsdir,"tlsfluctuations.png"), dpi=600, transparent=true);


## plot spectrotemporal charts
Γ1, Δf = qubit_v_time(tl, dt, f_TLS, g, f_qubit_all, Γ_qubit)

figure(5,figsize=(9,6)); clf()
heatmap(f_qubit_all.*1E-3, (0:dt:tl)./3600, inv.(Γ1),
    xlabel = "Qubit Frequency (GHz)", ylabel = "Time (h)", clabel="T₁ (μs)");
clim(0,T1_max);
savefig(joinpath(figsdir,"T1_v_time.png"), dpi=600, transparent=true);

#figure(6,figsize=(9,6)); clf()
#heatmap(f_qubit_all.*1E-3, (0:dt:tl)./3600, Δf,
#    xlabel = "Qubit Frequency (GHz)", ylabel = "Time (h)", clabel="Δf (MHz)", cmap="RdBu");
#l=maximum(abs,Δf); clim(-l,l);
#savefig(joinpath(figsdir,"Δf_v_time.png"), dpi=600, transparent=true);
